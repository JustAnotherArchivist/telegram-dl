FROM python:3.7-alpine3.14
RUN \
	apk add --no-cache gcc libc-dev libffi-dev \
	&& pip install --no-cache-dir telethon 'cryptg<0.3' tqdm
COPY telegram-dl.py /
VOLUME ["/data/"]
WORKDIR /data
ENTRYPOINT ["python3", "/telegram-dl.py"]
