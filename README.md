telegram-dl is a little script for retrieving content on Telegram.

Usage: `telegram-dl.py URL [URL...]`

All URLs must be messages in the same channel. The output consists of writing each message to a file `{channelName}_{messageId}.json`. If the message has any attachment (photo, video, file, etc.), it gets downloaded as well. The filename for that is chosen by Telethon; if it's a file attachment, the literal path as presented by Telegram is used, and if it's a photo or video, a name is made up.

The only mandatory dependency of the script is Telethon. You probably want cryptg as well for performance reasons when downloading larger files. With tqdm installed, you get a progress bar for each download.

For authentication (always required), specify the `TELEGRAM_API_ID`, `TELEGRAM_API_HASH`, and `TELEGRAM_BOT_TOKEN` environment variables. The session is stored in a file `.telegram-dl.session`; keeping this between executions is not critical but recommended.

A `Dockerfile` is provided for running with Docker. Simply provide the URL(s) as arguments on running the image. Data is written to the `/data` volume, which you may want to mount from the host machine instead.

    docker build -t telegram-dl:latest .
    docker run --rm -v "$(pwd)"/data:/data -e TELEGRAM_API_ID=... -e TELEGRAM_API_HASH=... -e TELEGRAM_BOT_TOKEN=... telegram-dl https://t.me/telegram/178
